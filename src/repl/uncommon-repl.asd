(cl:in-package #:asdf-user)

(defsystem #:uncommon-repl
  :depends-on (#:uncommon-core
	       #:inferior-shell
	       #:iup-scintilla)
  :serial t
  :components
  ((:file "package")
   (:file "repl")
   (:file "shell")))


(in-package #:uncommon-editor)

(defvar *editor* nil)

(alexandria:define-constant
    +default-callbacks+
    '(("map_cb" "uncommon-editor::init-editor")
      ("tabchange_cb" "uncommon-editor::buffer-map")
      ("k_any" "uncommon-editor::dispatch-key"))
  :test #'equal)

(defparameter *dracula-theme*
  (make-theme
   :id "Dracula"
   :bgcolor (make-rgb :red 40 :green 42 :blue 54)
   :fgcolor (make-rgb :red 248 :green 248 :blue 242)
   :numbers (make-rgb :red 189 :green 147 :blue 249 )

   :operators (make-rgb :red 248 :green 248 :blue 242)
   :keywords (make-rgb :red 255 :green 121 :blue 198)
   :strings (make-rgb :red 241 :green 250 :blue 140)
   :comments (make-rgb :red 98 :green 114 :blue 164)
   :caret (make-rgb :red 255 :green 255 :blue 255)))

(defclass editor ()
  ((buffer-table :initarg :buffer-table
		 :accessor editor-table
		 :type hash-table)
   (configuration :initarg :configuration
		  :accessor editor-configuration
		  :type list)
   (current-buffer :initarg :current-buffer
		   :accessor editor-current-buffer
		   :type (or buffer null))
   (tabs :initarg :tabs
	 :accessor editor-tabs
	 :type actor:box)
   (font :initarg :font
	 :accessor editor-font
	 :type string)))

(defun current-editor () *editor*)

(defgeneric get-buffer (editor id))

(defmethod get-buffer ((editor editor)
		       (iup-pointer tecgraf-base:ihandle))
  (gethash
   (iup:attribute iup-pointer "ID")
   (editor-table editor)))

(defgeneric add-buffer (editor file from))

;;TODO: Check for buffer name duplication (also maybe a better way to load big files than with alexandria)
(defmethod add-buffer ((editor editor) (file pathname) (from (eql :file)))
  (let* ((position (actor:get-attribute (editor-tabs editor) "COUNT"))
	 (buffer
	   (make-buffer
	    :id (if (pathname-type file)
		    (format nil "~a.~a" (pathname-name file) (pathname-type file))
		    (format nil "~a" (pathname-name file)))
	    :file file
	    :language *language-default*
	    :theme *dracula-theme*
	    :tab position
	    :attributes `(("expand" "yes")))))

    (alexandria:ensure-gethash
     (buffer-id buffer) (editor-table editor) buffer)

    (insert-tab editor buffer)
    (actor:update-item (editor-tabs editor)
		       `(("VALUEPOS" ,position)))

    (load-language buffer :default)
    (apply-theme buffer)
    (actor:update-item buffer `(("SAVEPOINT" "")))
    (insert buffer :current
	    (alexandria:read-file-into-string file))))

(defgeneric remove-buffer (editor id)) ;;Delete the tab too
(defgeneric save-buffer (editor id))
(defgeneric new-tab (editor tab-name buffer &key position))


(defgeneric insert-tab (tab buffer))

;;TODO: Remove the editor specialization
(defmethod insert-tab ((editor editor) (buffer buffer))
  (actor:append-item (editor-tabs editor) (buffer-window buffer))
  (actor:update-item (editor-tabs editor)
		     `((,(format nil "TABTITLE~a" (buffer-tab buffer))
			,(buffer-id buffer)))))

(defmethod insert-tab ((tabs actor:box) (buffer buffer))
  (actor:append-item tabs (buffer-window buffer))
  (actor:update-item tabs
		     `((,(format nil "TABTITLE~a" (buffer-tab buffer))
			,(buffer-id buffer)))))

(defun make-editor (&key buffer-table
			 configuration
			 (id (uuid:make-v1-uuid))
			 attributes
			 font)
  (make-instance 'editor
		 :buffer-table buffer-table
		 :current-buffer nil
		 :configuration configuration
		 :tabs (actor:make-box
			:type "tabs"
			:id id
			:attributes
			(append +default-callbacks+
				attributes))
		 :font font))

;;("STYLEFONT32" "Consolas") Font

;; Coger el buffer actual y hacer el dispatch del key-map
(actor:defcallback buffer-map (handle new_tab old_tab) ()
  (declare (ignore handle old_tab))
  (setf (editor-current-buffer *editor*)
	(get-buffer *editor* new_tab))

  ;;(load-keymap (editor-current-buffer *editor*) nil)
  )

;;TODO: Generalize this to support all kind of tabs
(actor:defcallback init-editor (handle) ()
  (declare (ignore handle))
  (maphash #'(lambda (name buffer)
	       (declare (ignore name))

	       (unless (editor-current-buffer *editor*)
		 (setf (editor-current-buffer *editor*) buffer))

	       (insert-tab *editor* buffer)
	       (load-language buffer :common-lisp)
	       (apply-theme buffer))
	   (editor-table *editor*)))

(defvar *last-key* nil)

;;TODO: Use the iup:+ignore+ to mimic the default behaviour BUT with our commands
;; instead of scintilla internally
(actor:defcallback dispatch-key (handle key) ()
  (declare (ignore handle))

  ;;TODO: Move all the logic to a general function (instead of only using this here)


  (actor:update-item (buffer-window
   		      (editor-current-buffer *editor*))
   		     `(("ANNOTATIONCLEARALL" "YES")))
  
  ;;TODO: Remove this, we are going to use for now ("usetabs" "no")
  ;; (when (= key (char-code #\Tab)); Hack to make the Tab work as intended, basically there is no tab
  ;;   (insert (editor-current-buffer *editor*)
  ;; 	    :current "       ")
  ;;   (return-from dispatch-key iup:+ignore+))

  ;;TODO: Probably optimize this, so it doesn't need to handle all this stuff if the
  ;; correct command key hasn't been press
  (let* ((buffer (editor-current-buffer *editor*))
	 (keymap (language-command-table (buffer-language buffer)))
	 (commands
	   (uncommon-core::key-map-commands keymap))

	 ;;TODO: This doesn't work, the way that scintilla behave regarding the modified keys
	 ;; is not this one, it assign a number to it (this can be see in uncommon-core::+control-combinations+)
	 (modifier-integer (and (integerp *last-key*)
				(uncommon-core::integer-to-modifier *last-key*))) 
	 (control-key
	   (and modifier-integer
		(find key
		      uncommon-core:+control-combinations+
		      :test #'=
		      :key #'car))))

    (alexandria:when-let* ((command
			    (and modifier-integer
				 (uncommon-core::get-command
				  commands
				  (or (cdr control-key) key)
				  modifier-integer)))
			   (command-fn (uncommon-core::command-fn command)))
      (funcall command-fn
	       :show
	       (funcall command-fn
			(language-id
			 (buffer-language (editor-current-buffer *editor*)))
			(command-action buffer
					(uncommon-core::command-argument command))))
      
      (return-from dispatch-key iup:+ignore+) ;Without this, scintilla may trigger their own command
      )
    (setf *last-key* key)))


(defun simple-editor ()
  "Creates and returns a simple editor."
  (let* ((buffer-list (list
		       (make-buffer :id "*scratch*"
				    :file nil
				    :theme *dracula-theme*
				    :tab "0"
				    :attributes
				    `(("expand" "yes")))))
	 (editor (make-editor :font nil
			      :buffer-table (make-hash-table :test #'equal)
			      :attributes
			      `(("expand" "yes")))))

    (loop for buffer in buffer-list
	  do (alexandria:ensure-gethash (buffer-id buffer)
					(editor-table editor)
					buffer))
    editor))

(defun show-editor (editor &optional menu)
  (actor:empty-virtual-dom)
  (let ((editor-tabs (editor-tabs editor))
	(output (actor:make-element :type "text"
				    :id "output"
				    :attributes
				    '(("bgcolor" "0 0 0")
				      ("expand" "HORIZONTAL")
				      ("multiline" "YES")
				      ("minsize" "100x140")))))
    (when menu (actor:map-dom-element menu))
    (actor:map-dom-element editor-tabs)
    (actor:map-dom-element output)
    
    (sb-int:with-float-traps-masked
	(:divide-by-zero :invalid)
      (iup:with-iup ()
	(iup-scintilla:open)
	(iup:show
	 (iup:dialog
	  ;; :menu (and menu
	  ;; 	     (eval (actor:handle-to-iup menu)))
	  (prog1 
	      (iup:vbox
	       (list (iup:hbox
		      (list 
		       (iup:button :title "Insert test" :action 'caret-insert)
		       (iup:button :title "File dialog" :action 'file-test)
		       (iup:button :title "Insert beginning test" :action 'caret-insert-beg)
		       (iup:button :title "Move caret beginning" :action 'caret-move-beg)
		       (iup:button :title "Thing at caret" :action 'tac)))
		     (eval (actor:handle-to-iup editor-tabs))
		     (eval (actor:handle-to-iup output)))))))
	(iup:main-loop))
      (actor:empty-virtual-dom))))

;; TEST ZONE

(actor:defcallback caret-insert (handle) nil
  (declare (ignore handle))
  (insert (editor-current-buffer *editor*) :current "test"))

(actor:defcallback file-test (handle) nil
  (declare (ignore handle))
  (let ((file (uncommon-core::file-dialog "Select file:" nil)))
    (add-buffer (current-editor) (pathname file) :file)
    (format t "FILEE ~A" file)))

(actor:defcallback caret-insert-beg (handle) nil
  (declare (ignore handle))
  (insert (editor-current-buffer *editor*) 0 "test"))


(actor:defcallback caret-move-beg (handle) nil
  (declare (ignore handle))
  (move-caret (editor-current-buffer *editor*) 0 ))


(actor:defcallback tac (handle) nil
  (declare (ignore handle))
  (let ((tac (thing-at-caret (editor-current-buffer *editor*) :word)))
    (actor:update-item (gethash "output" actor:*virtual-dom*)
		       `(("VALUE" ,tac)))))

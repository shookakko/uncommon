(in-package #:uncommon-editor)

(defvar *default-language-command-table*
  (uncommon-core::make-command-list
   '(("documentation" uncommon-editor::documentation (:control #\k) :prefix)
     ("autocomplete" uncommon-editor::autocomplete (:control #\u) :prefix)
     ("language-documentation" uncommon-editor::language-documentation (:control #\h) :prefix))))

;;TODO: Have a parameter and a configuration macro to attach configuration to language related things
;; like how to display certain things, colours and more language specific variables.

;; So, mimic this workflow:
;; Language A want to a configuration parameter for X
;; so it needs a configuration variables that have to be of the same type as X

;; When the user change the variable, it keep the value saved ensuring the type
(defclass language ()
  ((id :initarg :id
       :accessor language-id
       :type keyword)
   (lexer :initarg :lexer
	  :accessor language-lexer
	  :type string)
   (keywords :initarg :keywords
	     :accessor language-keywords
	     :type list)
   (command-table :initarg :command-table
		  :accessor language-command-table
		  :type key-map)
   (extensions :initarg :extensions
	       :accessor language-extensions
	       :type list)))

(defun make-language (&key id lexer keywords command-table extensions)
  (make-instance 'language
		 :id id
		 :lexer lexer
		 :keywords keywords
		 :command-table (or command-table *default-language-command-table*)
		 :extensions extensions))


(defgeneric autocomplete (language prefix))

(defgeneric editor-apropos (language prefix))

(defgeneric documentation (language symbol))

(defgeneric language-documentation (language symbol))

(defgeneric inline-doc (language symbol))

(defgeneric bformat (language))

(defgeneric references (language symbol))
(defgeneric goto-defintion (language symbol))
(defgeneric editor-inspect (language symbol))

(defgeneric concrete-tree (language buffer))

(defgeneric editor-eval (language expression type))
(defgeneric editor-compile (language expression type))

(defgeneric editor-compile-file (language buffer))

;;Also some lisp specific functions (like macroexpand) 


;;AKA minor-mode (maomeno) also things like paredit
;; (defclass language-extension () ())

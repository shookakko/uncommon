(cl:in-package #:asdf-user)

(defsystem #:uncommon
  :version "0.0.1"
  :description "A not so common Common Lisp IDE"
  :author "Fermin MF <fmfs@posteo.net>"
  :license "GNU GPLv3"
  :depends-on (#:uncommon-core
	       #:uncommon-configuration
	       #:uncommon-editor
	       #:uncommon-repl
	       ;; #:uncommon-project
	       ;; #:uncommon-sidebar
	       ;; #:uncommon-helpbar
	       #:uncommon-run
	       ))

* Mastermind
  
  The idea is to build an autocomplete engine that is able to display
  different source at once, or just one depending on the user needs.
  

** Sources  
*** Files (project files)
    By file type and name

*** References (in Common Lisp, this would be symbols)

*** Commands (basically actions)

*** Grep like (find)

*** Actions (similar to commands, but no specific functions but things like run tests and similar)

** Autocomplete methods
*** Prefix autocompletion

*** Fuzzy autocompletion

*** Guess autocompletion (experimental)

** Display
*** Position configuration
   
** Extensibility
*** Generic to extend the functionality so other extension can add more sources
   
** Optimizations
